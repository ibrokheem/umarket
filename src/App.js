import React, { useEffect } from "react";
import "./App.css";
import { BrowserRouter, Link, Route, Routes, Switch } from "react-router-dom";
import ApplicationBanner from "./components/ApplicationBanner/ApplicationBanner";
import Banner from "./components/Banner/Banner";
import Categories from "./components/Categories/Categories";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import HomeAppliances from "./components/HomeAppliances/HomeAppliances";
import Navbar from "./components/Navbar/Navbar";
import PopularBrands from "./components/PopularBrands/PopularBrands";
import ProductsList from "./components/ProductsList/ProductsList";
import Topbar from "./components/Topbar/Topbar";
import Delivery from "./screens/Delivery/Delivery";
import Login from "./screens/Login/Login";
import Review from "./screens/Review/Review";
import Shops from "./screens/Shops/Shops";

// ! import request instead of axios
import { request } from "./utils/request";
import SignIn from "./components/SignIn/SignIn";
import Home from "./screens/Home/Home";
import SignUp from "./components/SignUp/SignUp";
import Cart from "./screens/Cart/Cart";
import Formalize from "./screens/Formalize/Formalize";
import Saved from "./screens/Saved/Saved";
import ProductPage from "./screens/ProductPage/ProductPage";

import PhoneImg from "./Assets/images/smartphones.png";
import PhoneImg2 from "./Assets/images/samsun-a-41.png";
import PhoneImg3 from "./Assets/images/honor.png";

import MonitorImg from "./Assets/images/monitors.png";
import ComputerImg from "./Assets/images/computers.png";
import AccessoriesImg from "./Assets/images/accessories.png";
import CookerImg from "./Assets/images/cooker.png";
import VacuumCleanerImg from "./Assets/images/vacuum-cleaner.png";
import WashingMachineImg from "./Assets/images/washing-machine.png";
import FreedgeImg from "./Assets/images/freedge.png";
import ConditionerImg from "./Assets/images/conditioner.png";
import { useState } from "react";
import CounterProvider from "./contexts/cartProductCounter";

const App = () => {
  // const getUsers = () => {
  //   request
  //     .get("/users")
  //     .then((res) => {
  //       console.log("res", res);
  //     })
  //     .catch((err) => {
  //       console.log("err", err);
  //     });
  // };

  // useEffect(() => {
  //   getUsers();
  // }, []);

  const favourites = [
    {
      id: 1,
      name: "Samsung Galaxy A41 Red",
      img: PhoneImg2,
      comments: 23,
      rating: 5,
      price: 450,
      color: "red",
      memory: "64GB",
      credit: 450,
      available: true,
      inCart: true,
      inFavourite: false,
      inCompare: false,
      quantity: 1,
      isChecked: false,
    },
    {
      id: 2,
      name: "Samsung Galaxy A41 Red",
      img: PhoneImg2,
      comments: 23,
      rating: 5,
      price: 450,
      color: "red",
      memory: "64GB",
      credit: 450,
      available: true,
      inCart: true,
      inFavourite: false,
      inCompare: false,
      quantity: 1,
      isChecked: false,
    },
    {
      id: 3,
      name: "Samsung Galaxy A41 Red",
      img: PhoneImg2,
      comments: 23,
      rating: 5,
      price: "3 144 000 сум",
      color: "red",
      memory: "64GB",
      credit: "385 000 сум",
      available: true,
      inCart: false,
      inFavourite: false,
      inCompare: false,
      quantity: 1,
      isChecked: false,
    },
    {
      id: 4,
      name: "Honor P50 lite Black",
      img: PhoneImg3,
      comments: 23,
      rating: 5,
      price: "3 144 000 сум",
      color: "red",
      memory: "64GB",
      credit: "385 000 сум",
      available: true,
      inCart: false,
      inFavourite: false,
      inCompare: false,
      quantity: 1,
      isChecked: false,
    },
  ];

  // localStorage.setItem(
  //   "cart",
  //   JSON.stringify([
  //     {
  //       id: 1,
  //       name: "Samsung Galaxy A41 Red",
  //       img: PhoneImg2,
  //       comments: 23,
  //       rating: 5,
  //       price: 450,
  //       color: "red",
  //       memory: "64GB",
  //       credit: 450,
  //       available: true,
  //       inCart: true,
  //       inFavourite: false,
  //       inCompare: false,
  //       quantity: 1,
  //       isChecked: false,
  //     },
  //     {
  //       id: 2,
  //       name: "Samsung Galaxy A41 Red",
  //       img: PhoneImg2,
  //       comments: 23,
  //       rating: 5,
  //       price: 450,
  //       color: "red",
  //       memory: "64GB",
  //       credit: 450,
  //       available: true,
  //       inCart: true,
  //       inFavourite: false,
  //       inCompare: false,
  //       quantity: 1,
  //       isChecked: false,
  //     },
  //   ])
  // );

  // localStorage.setItem('salesHits', JSON.stringify([
  //   {
  //     id: 1,
  //     name: "Samsung Galaxy A41 Red",
  //     img: PhoneImg2,
  //     comments: 23,
  //     rating: 5,
  //     priceUZS: "3 144 000 сум",
  //     price: 350,
  //     color: "red",
  //     memory: "64GB",
  //     credit: "385 000 сум",
  //     available: true,
  //     inCart: false,
  //     inFavourite: false,
  //     inCompare: false,
  //     quantity: 1,
  //     isChecked: false,
  //   },
  //   {
  //     id: 2,
  //     name: "Samsung Galaxy A41 Red",
  //     img: PhoneImg2,
  //     comments: 23,
  //     rating: 5,
  //     price: 350,
  //     priceUZS: "3 144 000 сум",
  //     color: "red",
  //     memory: "64GB",
  //     credit: "385 000 сум",
  //     available: true,
  //     inCart: false,
  //     inFavourite: false,
  //     inCompare: false,
  //     quantity: 1,
  //     isChecked: false,
  //   },
  //   {
  //     id: 3,
  //     name: "Samsung Galaxy A41 Red",
  //     img: PhoneImg2,
  //     comments: 23,
  //     rating: 5,
  //     price: 350,
  //     priceUZS: "3 144 000 сум",
  //     color: "red",
  //     memory: "64GB",
  //     credit: "385 000 сум",
  //     available: true,
  //     inCart: false,
  //     inFavourite: false,
  //     inCompare: false,
  //     quantity: 1,
  //     isChecked: false,
  //   },
  //   {
  //     id: 4,
  //     name: "Honor P50 lite Black",
  //     img: PhoneImg3,
  //     comments: 23,
  //     rating: 5,
  //     price: 400,
  //     priceUZS: "4 500 000 сум",
  //     color: "red",
  //     memory: "64GB",
  //     credit: "385 000 сум",
  //     available: true,
  //     inCart: false,
  //     inFavourite: false,
  //     inCompare: false,
  //     quantity: 1,
  //     isChecked: false,
  //   },
  // ]))


  let localStrgCart = localStorage.getItem("cart");
  localStrgCart = JSON.parse(localStrgCart);

  const [cart, setCart] = useState(localStrgCart || []);

  const categories = [
    {
      img: PhoneImg,
      title: "Смартфоны",
    },
    {
      img: MonitorImg,
      title: "Мониторы",
    },
    {
      img: ComputerImg,
      title: "Компьютеры",
    },
    {
      img: AccessoriesImg,
      title: "Аксессуары",
    },
  ];

  let localStrgSalesHits = localStorage.getItem("salesHits");
  localStrgSalesHits = JSON.parse(localStrgSalesHits);

  const [salesHits, setSalesHits] = useState(
    localStrgSalesHits || [
      {
        id: 1,
        name: "Samsung Galaxy A41 Red",
        img: PhoneImg2,
        comments: 23,
        rating: 5,
        priceUZS: "3 144 000 сум",
        price: 350,
        color: "red",
        memory: "64GB",
        credit: "385 000 сум",
        available: true,
        inCart: false,
        inFavourite: false,
        inCompare: false,
        quantity: 1,
        isChecked: false,
      },
      {
        id: 2,
        name: "Samsung Galaxy A41 Red",
        img: PhoneImg2,
        comments: 23,
        rating: 5,
        price: 350,
        priceUZS: "3 144 000 сум",
        color: "red",
        memory: "64GB",
        credit: "385 000 сум",
        available: true,
        inCart: false,
        inFavourite: false,
        inCompare: false,
        quantity: 1,
        isChecked: false,
      },
      {
        id: 3,
        name: "Samsung Galaxy A41 Red",
        img: PhoneImg2,
        comments: 23,
        rating: 5,
        price: 350,
        priceUZS: "3 144 000 сум",
        color: "red",
        memory: "64GB",
        credit: "385 000 сум",
        available: true,
        inCart: false,
        inFavourite: false,
        inCompare: false,
        quantity: 1,
        isChecked: false,
      },
      {
        id: 4,
        name: "Honor P50 lite Black",
        img: PhoneImg3,
        comments: 23,
        rating: 5,
        price: 400,
        priceUZS: "4 500 000 сум",
        color: "red",
        memory: "64GB",
        credit: "385 000 сум",
        available: true,
        inCart: false,
        inFavourite: false,
        inCompare: false,
        quantity: 1,
        isChecked: false,
      },
    ]
  );

  const homeAppliances = [
    {
      img: CookerImg,
      title: "Встраиваемая техника",
    },
    {
      img: VacuumCleanerImg,
      title: "Пылесосы",
    },
    {
      img: WashingMachineImg,
      title: "Стиральные машины",
    },
    {
      img: FreedgeImg,
      title: "Холодильники",
    },
    {
      img: ConditionerImg,
      title: "Кондиционеры",
    },
  ];

  return (
    <CounterProvider>
      <BrowserRouter>
        <Topbar />
        <Header cart={cart} />
        <Navbar />

        <Routes>
          <Route
            exact
            path="/"
            element={
              <Home
                categories={categories}
                salesHits={salesHits}
                setSalesHits={setSalesHits}
                homeAppliances={homeAppliances}
                cart={cart}
                setCart={setCart}
              />
            }
          />
          <Route exact path="/shops" element={<Shops />} />
          <Route exact path="/review" element={<Review />} />
          <Route exact path="/delivery" element={<Delivery />} />
          <Route
            exact
            path="/cart"
            element={<Cart cart={cart} setCart={setCart} salesHits={salesHits} setSalesHits={setSalesHits}/>}
          />
          <Route exact path="/signin" element={<SignIn />} />
          <Route exact path="/formalize" element={<Formalize data={cart} />} />
          <Route exact path="/saved" element={<Saved />} />
          <Route exact path="/product-page" element={<ProductPage />} />
        </Routes>

        <Footer />
      </BrowserRouter>
    </CounterProvider>
  );
};

export default App;
