import React from "react";
import "./product-page.css";
import { Link } from "react-router-dom";
import Avatar from "../../Assets/images/avatar.png";
import Breadcrumb from "../../components/Breadcrumb/Breadcrumb";
import PhoneImg from "../../Assets/images/samsun-a-41.png";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import Navbar from "../../components/Navbar/Navbar";
import Topbar from "../../components/Topbar/Topbar";
import ProductsList from "../../components/ProductsList/ProductsList";
import { Tabs, TabLink, TabContent } from "react-tabs-redux";

const ProductPage = () => {
  return (
    <div className="product-page">
      <div className="container product-page__container">
        <Breadcrumb />

        <div className="product-page__header">
          <h2 className="product-page__name">Samsung Galaxy A41</h2>

          <div className="products-item__rating-wrapper product-modal__rating-wrapper">
            <span className="products-item__rating product-modal__rating"></span>
            <span className="products-item__rating product-modal__rating"></span>
            <span className="products-item__rating product-modal__rating"></span>
            <span className="products-item__rating product-modal__rating"></span>
            <span className="products-item__rating product-modal__rating"></span>

            <span className="products-item__hashtag">Хит продажи</span>

            <span className="products-item__hashtag">23 отзывов</span>
          </div>
        </div>

        <div className="product-page__main-info">
          <div className="product-page__properties">
            <ul className="product-page__additional-images-list">
              <li className="product-page__additional-images-item">
                <img
                  className="product-page__additional-images-img"
                  src={PhoneImg}
                  alt="product"
                  width={60}
                  height={60}
                />
              </li>
              <li className="product-page__additional-images-item">
                <img
                  className="product-page__additional-images-img"
                  src={PhoneImg}
                  alt="product"
                  width={60}
                  height={60}
                />
              </li>

              <li className="product-page__additional-images-item">
                <img
                  className="product-page__additional-images-img"
                  src={PhoneImg}
                  alt="product"
                  width={60}
                  height={60}
                />
              </li>
              <li className="product-page__additional-images-item">
                <img
                  className="product-page__additional-images-img"
                  src={PhoneImg}
                  alt="product"
                  width={60}
                  height={60}
                />
              </li>
            </ul>
            <div className="product-page__main-image-wrapper">
              <img src={PhoneImg} alt="product" width={244} height={323} />
              <a className="product-page__favourite-icon" href="">
                <svg
                  width="21"
                  height="20"
                  viewBox="0 0 21 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_305_3983)">
                    <path
                      d="M18.3183 3.84172C17.8926 3.41589 17.3873 3.0781 16.8311 2.84763C16.2748 2.61716 15.6787 2.49854 15.0766 2.49854C14.4745 2.49854 13.8784 2.61716 13.3221 2.84763C12.7659 3.0781 12.2606 3.41589 11.8349 3.84172L10.9516 4.72506L10.0683 3.84172C9.20852 2.98198 8.04246 2.49898 6.8266 2.49898C5.61074 2.49898 4.44467 2.98198 3.58493 3.84172C2.72519 4.70147 2.24219 5.86753 2.24219 7.08339C2.24219 8.29925 2.72519 9.46531 3.58493 10.3251L4.46826 11.2084L10.9516 17.6917L17.4349 11.2084L18.3183 10.3251C18.7441 9.89943 19.0819 9.39407 19.3124 8.83785C19.5428 8.28164 19.6615 7.68546 19.6615 7.08339C19.6615 6.48132 19.5428 5.88514 19.3124 5.32893C19.0819 4.77271 18.7441 4.26735 18.3183 3.84172V3.84172Z"
                      stroke="currentColor"
                      stroke-width="2"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_305_3983">
                      <rect
                        width="20"
                        height="20"
                        fill="currentColor"
                        transform="translate(0.95166)"
                      />
                    </clipPath>
                  </defs>
                </svg>
              </a>
              <a className="product-page__compare-icon" href="#">
                <svg
                  width="21"
                  height="20"
                  viewBox="0 0 21 20"
                  fill=""
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M18.785 6.66671L15.4517 3.33337V5.83337H2.95166V7.50004H15.4517V10L18.785 6.66671Z"
                    fill="currentColor"
                  />
                  <path
                    d="M2.11841 13.3333L5.45174 16.6667V14.1667H17.9517V12.5H5.45174V10L2.11841 13.3333Z"
                    fill="currentColor"
                  />
                </svg>
              </a>
            </div>
          </div>

          <div className="product-page__info">
            <form
              className="product-page__form"
              action="https://echo.htmlacademy.ru"
              method="Post"
            >
              <div className="product-page__form-inner">
                <div className="product-page__colors-list">
                  <p className="product-page__text">Цвет</p>
                  <div className="product-page__labels-wrapper">
                    <label className="product-page__colors-label">
                      <input
                        className="product-page__colors-input visually-hidden"
                        type="radio"
                        name="color"
                        value="red"
                      />
                      <span className="product-page__colors-custom-input tomato"></span>
                    </label>

                    <label className="product-page__colors-label">
                      <input
                        className="product-page__colors-input visually-hidden"
                        type="radio"
                        name="color"
                        value="grey"
                      />
                      <span className="product-page__colors-custom-input grey"></span>
                    </label>

                    <label className="product-page__colors-label">
                      <input
                        className="product-page__colors-input visually-hidden"
                        type="radio"
                        name="color"
                        value="blue"
                      />
                      <span className="product-page__colors-custom-input blue"></span>
                    </label>
                  </div>
                </div>

                <div className="product-page__memories-list">
                  <p className="product-page__txt">Память</p>
                  <div className="product-page__labels-wrapper">
                    <label className="product-page__memories-label">
                      <input
                        className="product-page__memories-input visually-hidden"
                        type="radio"
                        name="memory"
                        value="64"
                      />
                      <span className="product-page__memories-custom-input">
                        64Gb
                      </span>
                    </label>
                    <label className="product-page__memories-label">
                      <input
                        className="product-page__memories-input visually-hidden"
                        type="radio"
                        name="memory"
                        value="128"
                      />
                      <span className="product-page__memories-custom-input">
                        128GB
                      </span>
                    </label>
                    <label className="product-page__memories-label">
                      <input
                        className="product-page__memories-input visually-hidden"
                        type="radio"
                        name="memory"
                        value="256"
                      />
                      <span className="product-page__memories-custom-input">
                        256GB
                      </span>
                    </label>
                  </div>
                </div>

                <div className="product-page__sim-list">
                  <p className="product-page__txt">SIM</p>
                  <label className="product-page__sim-label">
                    <input
                      className="product-page__sim-input visually-hidden"
                      type="radio"
                      name="sim"
                      value="dual"
                    />
                    <span className="product-page__sim-custom-input">Dual</span>
                  </label>
                  <label className="product-page__sim-label">
                    <input
                      className="product-page__sim-input visually-hidden"
                      type="radio"
                      name="sim"
                      value="single"
                    />
                    <span className="product-page__sim-custom-input">
                      Single
                    </span>
                  </label>
                </div>
              </div>

              <div className="product-page-summary">
                <dl className="product-page__information-list">
                  <div className="product-page__information-wrapper price">
                    <dt className="product-page__information-title">
                      Цена: &nbsp;
                    </dt>
                    <dd className="product-page__information-def">
                      {" "}
                      12 972 000 сум
                    </dd>
                  </div>

                  <div className="product-page__information-wrapper">
                    <dt className="product-page__information-title">
                      Рассрочка от: &nbsp;
                    </dt>
                    <dd className="product-page__information-def">
                      1 300 000 сум/2 мес
                    </dd>
                  </div>

                  <label>
                    <input
                      className="product-page__chkb"
                      type="checkbox"
                      name="sale"
                    />
                    Получить со скидкой
                  </label>
                </dl>

                <button className="product-page__submit-btn" type="submit">
                  Добавить в корзину
                </button>

                <p className="product-page__txt">
                  Цены на сайте могут отличаться от цен в наших магазинах.
                </p>

                <p className="product-page__txt">
                  Подробно по номеру 71 282-34-34{" "}
                </p>
              </div>
            </form>
          </div>
        </div>

        <Tabs className="product-page__tabs">
          <TabLink className="product-page__tablink" to="tab1">
            О товаре
          </TabLink>

          <TabLink className="product-page__tablink" to="tab2">
            Наличие в магазинах
          </TabLink>

          <TabLink className="product-page__tablink" to="tab3" default>
            Отзывы
          </TabLink>

          <TabContent className="product-page__tabcontent" for="tab1">
            <div className="product-page__tabcontent__about-product">
              <p className="product-page__tabcontent__about-text">
                Samsung Galaxy A52 <br /> Потрясающий экран, реальная плавная
                прокрутка. Отличная камера, всегда резкая и стабильная.
                Полюбуйтесь яркими деталями на дисплее FHD+ Super AMOLED с
                яркостью 800 нит¹ для четкости даже при ярком дневном свете...
              </p>

              <dl className="product-page__tabcontent__def-list">
                <div className="product-page__tabcontent__def-inner">
                  <dt className="product-page__tabcontent__def-title">
                    Измерение
                  </dt>

                  <span className="product-page__line"></span>

                  <dd className="product-page__tabcontent__def-desc">
                    159,9 х 75,1 х 8,4 мм
                  </dd>
                </div>

                <div className="product-page__tabcontent__def-inner">
                  <dt className="product-page__tabcontent__def-title">Масса</dt>

                  <span className="product-page__line"></span>

                  <dd className="product-page__tabcontent__def-desc">189 г</dd>
                </div>

                <div className="product-page__tabcontent__def-inner">
                  <dt className="product-page__tabcontent__def-title">
                    Батарея
                  </dt>

                  <span className="product-page__line"></span>

                  <dd className="product-page__tabcontent__def-desc">
                    Несъемный литий-полимерный аккумулятор емкостью 4500 мАч.
                  </dd>
                </div>

                <div className="product-page__tabcontent__def-inner">
                  <dt className="product-page__tabcontent__def-title">
                    Операционные системы
                  </dt>

                  <span className="product-page__line"></span>

                  <dd className="product-page__tabcontent__def-desc">
                    DDT 1098
                  </dd>
                </div>

                <div className="product-page__tabcontent__def-inner">
                  <dt className="product-page__tabcontent__def-title">
                    Память
                  </dt>

                  <span className="product-page__line"></span>

                  <dd className="product-page__tabcontent__def-desc">
                    Встроенная память 128 ГБ, слот для оперативной памяти 8 ГБ
                    microSDXC
                  </dd>
                </div>
              </dl>
            </div>
          </TabContent>

          <TabContent className="product-page__tabcontent" for="tab2">
            <table className="product-page__table">
              <thead className="product-page__thead">
                <tr className="product-page__thead__tr">
                  <th className="product-page__th">Магазин</th>
                  <th className="product-page__th">Адрес</th>
                  <th className="product-page__th">Режим работы</th>
                  <th className="product-page__th">Наличие в магазине</th>
                </tr>
              </thead>

              <tbody className="product-page__tbody">
                <tr className="product-page__thbody__tr">
                  <td className="product-page__td">Самарканд</td>
                  <td className="product-page__td">
                    г. Ташкент, Мирзо Улугбекский р-н, проспект Мустакиллик, 7
                  </td>
                  <td className="product-page__td">
                    с 09:00 до 21:00 (без выходных)
                  </td>
                  <td className="product-page__td">Мало</td>
                </tr>

                <tr className="product-page__thbody__tr">
                  <td className="product-page__td">Бухара</td>
                  <td className="product-page__td">
                    г. Бухара, улица Ибн Сино 4
                  </td>
                  <td className="product-page__td">
                    с 09:00 до 21:00 (без выходных)
                  </td>
                  <td className="product-page__td">Нет</td>
                </tr>
              </tbody>
            </table>
          </TabContent>

          <TabContent className="product-page__tabcontent" for="tab3">
            <button className="product-page__comment-btn">
              Написать отзыв
            </button>

            <ol className="product-page__comments">
              <li className="product-page__comments-item">
                <div className="product-page__comments-item-header">
                  <img
                    className="product-page__comments__user-pic"
                    src={Avatar}
                    alt="user-pic"
                    width={40}
                    height={40}
                  />
                  <span className="product-page__username">Adrian Harvey</span>

                  <time className="product-page__time" dateTime="2022-03-22">
                    22.03.2021
                  </time>
                </div>

                <div className="product-page__comments-item-content">
                  <p className="product-page__comments-text">
                    Это уже третий iphone, заказанный посредством udevsmarket.
                    Заказ пришел раньше заявленного срока!👍Пломбы на месте,
                    аппарат идеальный, оригинальный вне всяких сомнений.
                    Рекомендую
                  </p>
                </div>

                <div className="product-page__comments-item-reactions">
                  <button className="product-page__comments-item-btn-like">
                    10
                  </button>

                  <button className="product-page__comments-item-btn-dislike">
                    4
                  </button>
                </div>
              </li>

              <li className="product-page__comments-item">
                <div className="product-page__comments-item-header">
                  <img
                    className="product-page__comments__user-pic"
                    src={Avatar}
                    alt="user-pic"
                    width={40}
                    height={40}
                  />
                  <span className="product-page__username">Adrian Harvey</span>

                  <time className="product-page__time" dateTime="2022-03-22">
                    22.03.2021
                  </time>
                </div>

                <div className="product-page__comments-item-content">
                  <p className="product-page__comments-text">
                    Это уже третий iphone, заказанный посредством udevsmarket.
                    Заказ пришел раньше заявленного срока!👍Пломбы на месте,
                    аппарат идеальный, оригинальный вне всяких сомнений.
                    Рекомендую
                  </p>
                </div>

                <div className="product-page__comments-item-reactions">
                  <button className="product-page__comments-item-btn-like">
                    10
                  </button>

                  <button className="product-page__comments-item-btn-dislike">
                    4
                  </button>
                </div>
              </li>

              <li className="product-page__comments-item">
                <div className="product-page__comments-item-header">
                  <img
                    className="product-page__comments__user-pic"
                    src={Avatar}
                    alt="user-pic"
                    width={40}
                    height={40}
                  />
                  <span className="product-page__username">Adrian Harvey</span>

                  <time className="product-page__time" dateTime="2022-03-22">
                    22.03.2021
                  </time>
                </div>

                <div className="product-page__comments-item-content">
                  <p className="product-page__comments-text">
                    Это уже третий iphone, заказанный посредством udevsmarket.
                    Заказ пришел раньше заявленного срока!👍Пломбы на месте,
                    аппарат идеальный, оригинальный вне всяких сомнений.
                    Рекомендую
                  </p>
                </div>

                <div className="product-page__comments-item-reactions">
                  <button className="product-page__comments-item-btn-like">
                    10
                  </button>

                  <button className="product-page__comments-item-btn-dislike">
                    4
                  </button>
                </div>
              </li>
            </ol>
          </TabContent>
        </Tabs>
      </div>

      <ProductsList title={"Сопуствующие товары"}></ProductsList>
    </div>
  );
};

export default ProductPage;
