import React from "react";
import { useDispatch, useSelector } from "react-redux";
import ApplicationBanner from "../../components/ApplicationBanner/ApplicationBanner";
import Banner from "../../components/Banner/Banner";
import Categories from "../../components/Categories/Categories";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import HomeAppliances from "../../components/HomeAppliances/HomeAppliances";
import Navbar from "../../components/Navbar/Navbar";
import PopularBrands from "../../components/PopularBrands/PopularBrands";
import ProductsList from "../../components/ProductsList/ProductsList";
import Topbar from "../../components/Topbar/Topbar";
import Delivery from "../../screens/Delivery/Delivery";
import Login from "../../screens/Login/Login";
import Review from "../../screens/Review/Review";
import Shops from "../../screens/Shops/Shops";
import { showProductItem } from "../../redux/actions/passProductInfo";
import SignUp from "../../components/SignUp/SignUp";
import ProductModal from "../../components/ProductModal/ProductModal";
import ProductPage from "../ProductPage/ProductPage";

const Home = ({
  categories,
  salesHits,
  setSalesHits,
  homeAppliances,
  cart,
  setCart,
}) => {
  return (
    <>
      <main>
        <h1 className="visually-hidden">
          Umarket online shopping for everyone
        </h1>
        <Banner />
        <Categories title={"Популярные категории"} data={categories} />
        <ProductsList
          title={"Хиты продаж"}
          products={salesHits}
          setProducts={setSalesHits}
          cart={cart}
          setCart={setCart}
        />
        <HomeAppliances title={"Техника для дома"} data={homeAppliances} />
        <ProductsList
          title={"Выбор покупателей"}
          products={salesHits}
          setProducts={setSalesHits}
          cart={cart}
          setCart={setCart}
        />
        <PopularBrands title={"Популярные бренды"} />
        <ApplicationBanner />
      </main>
    </>

    // <ProductPage></ProductPage>
  );
};
export default Home;
