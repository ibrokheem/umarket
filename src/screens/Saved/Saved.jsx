import React from "react";
import "./saved.css";
import ApplicationBanner from "../../components/ApplicationBanner/ApplicationBanner";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import Navbar from "../../components/Navbar/Navbar";
import ProductsList from "../../components/ProductsList/ProductsList";
import Topbar from "../../components/Topbar/Topbar";

const Saved = () => {
  return (
    <div className="saved">
      <div className="container saved__container">
        <ProductsList title={"Избранные"} />
      </div>
      <ApplicationBanner />
    </div>
  );
};

export default Saved;
