import React from "react";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "./cart.css";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import Navbar from "../../components/Navbar/Navbar";
import Topbar from "../../components/Topbar/Topbar";
import ProductsList from "../../components/ProductsList/ProductsList";
import CartProduct from "../../components/CartProduct/CartProduct";

const Cart = ({ cart, setCart, salesHits, setSalesHits }) => {
  const [choosenItems, setChoosenItems] = useState(
    cart.filter((item) => item.isChecked === true).length
  );
  
  let [totalSum, setTotal] = useState(0);

  const handleChecboxChange = () => {
    setChoosenItems(cart.filter((item) => item.isChecked === true).length);
  };

  const calculateTotal = () => {
    let counter = 0;
    if (cart.length > 0) {
      for (let item of cart) {
        counter += item.price * item.quantity;
        setTotal(counter);
      }
    } else {
      setTotal(0);
    }
  };

  const deleteChosenItems = () => {
    setCart(cart.filter((item) => !item.isChecked));
    calculateTotal();
  };

  useEffect(() => {
    handleChecboxChange();
    calculateTotal();
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  return (
    <div className="cart">
      <form className="container cart__container" method="POST">
        <div className="cart__header">
          <h2 className="cart__headline">Корзина</h2>
          <p className="cart__quantity">Число товаров: {cart.length}</p>
          <button
            className="cart__delete-btn"
            type="button"
            onClick={choosenItems ? deleteChosenItems : null}
          >
            Удалить выбранные ({choosenItems})
          </button>
        </div>

        <div className="cart__content">
          <div className="cart__left">
            <ul className="cart__list" onChange={handleChecboxChange}>
              {cart.map((item) => (
                <CartProduct
                  cart={cart}
                  item={item}
                  key={item.id}
                  setCart={setCart}
                  salesHits={salesHits}
                  setSalesHits={setSalesHits}
                  calculateTotal={calculateTotal}
                ></CartProduct>
              ))}
            </ul>
          </div>

          <div className="cart__right">
            <div className="cart__right-header">
              <h2 className="cart__right-headline">Итого</h2>
              <span className="cart__right-price">${totalSum}</span>
            </div>

            <dl className="cart__information-list">
              <div className="cart__information-wrapper">
                <dt className="cart__information-title">Товары: &nbsp;</dt>
                <dd className="cart__information-def">{cart.length} шт</dd>
              </div>

              <div className="cart__information-wrapper">
                <dt className="cart__information-title">Скидка: &nbsp;</dt>
                <dd className="cart__information-def">$0</dd>
              </div>

              <div className="cart__information-wrapper">
                <dt className="cart__information-title">Доставка: &nbsp;</dt>
                <dd className="cart__information-def">
                  <Link className="cart__link" to={"/delivery"}>
                    Выбрать адрес доставки
                  </Link>
                </dd>
              </div>
            </dl>

            <Link
              className={`cart__submit-btn ${
                totalSum <= 0 ? "cart__submit-btn--disabled" : ""
              }`}
              to="/formalize"
              onClick={totalSum <= 0 ? (e) => e.preventDefault() : null}
            >
              Перейти к оформлению
            </Link>

            <p className="cart__txt">
              Заказывая соглашаетесь с условиями пользовании торговой площадкой
            </p>
          </div>
        </div>

        {/* <ProductsList title="Ещё вам может понравится" /> */}
      </form>
    </div>
  );
};

export default Cart;
