import React from "react";
import SignIn from "../../components/SignIn/SignIn";
import SignUp from "../../components/SignUp/SignUp";
import "./login.css";
import { useState } from "react";

const Login = ({ isRegistered = false, setIsRegistered, show, setShowLoginModal }) => {
  return (
    <>
      {isRegistered ? (
        <SignIn show={show} setIsRegistered={setIsRegistered}  setShowLoginModal={setShowLoginModal}></SignIn>
      ) : (
        <SignUp show={show} setIsRegistered={setIsRegistered} setShowLoginModal={setShowLoginModal}></SignUp>
      )}
    </>
  );
};

export default Login;
