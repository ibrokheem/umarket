import React from "react";
import "./review.css";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import Navbar from "../../components/Navbar/Navbar";
import Topbar from "../../components/Topbar/Topbar";

const Review = () => {
  return (
    <div className="review">
      <div className="container">
        <div className="review__container">
          <h2 className="review__headline">Ваш отзыв о Goodzone</h2>
          <p className="review__desc">
            Благодарим Вас за то, что Вы посетили наш интернет-магазин. Мы
            внимательно отслеживаем все этапы работы магазинов нашей торговой
            сети и будем очень признательны, если вы оставите отзыв о нашей
            работе.
          </p>

          <form
            className="review__form"
            action="https://echo.htmlacademy.ru"
            method="Post"
          >
            <label className="review__label">
              <span className="review__label-name">Ф.И.О.</span>
              <input
                className="review__input"
                type="text"
                name="full_name"
                placeholder="Напишите полное имя"
              />
            </label>

            <label className="review__label">
              <span className="review__label-name">Электронный адрес</span>
              <input
                className="review__input"
                type="email"
                name="email"
                placeholder="Напишите электронный адрес"
              />
            </label>

            <label className="review__label">
              <span className="review__label-name">Номер телефона</span>
              <input
                className="review__input"
                type="text"
                name="phone_number"
                defaultValue={"+998"}
                maxLength={13}
              />
            </label>

            <label className="review__label">
              <span className="review__label-name">Магазин</span>
              <select
                className="review__input review__select"
                placeholder="Выберите  магазин"
                name="shop"
              >
                <option selected disabled>
                  Выберите магазин
                </option>
                <option value="sergeli1">Goodzone Сергели</option>
                <option value="sergeli2">Goodzone Сергели</option>
                <option value="sergeli3">Goodzone Сергели</option>
              </select>
            </label>

            <label className="review__label">
              <span className="review__label-name">Тип отзыва</span>
              <select
                className="review__input review__select"
                name="type_of_review"
              >
                <option className="option" selected disabled>
                  Выберите тип
                </option>
                <option value="sergeli1">Goodzone Сергели</option>
                <option value="sergeli2">Goodzone Сергели</option>
                <option value="sergeli3">Goodzone Сергели</option>
              </select>
            </label>

            <label className="review__label">
              <span className="review__label-name">Комментарии</span>
              <textarea
                className="review__input review__textarea"
                name="comment"
                cols="30"
                rows="10"
                placeholder="Ваш комментарий"
              ></textarea>
            </label>

            <button className="review__submit-btn" type="submit">
              Отправить
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Review;
