import React from "react";
import "./delivery.css";
import Topbar from "../../components/Topbar/Topbar";
import Header from "../../components/Header/Header";
import Navbar from "../../components/Navbar/Navbar";
import Footer from "../../components/Footer/Footer";
import DeliveryItem from "../../components/DeliveryItem/DeliveryItem";

const Delivery = () => {
  return (
    <div className="delivery">
      <div className="container delivery__container">
        <div className="delivery__welcome">
          <h2 className="delivery__headline">Доставка</h2>

          <p className="delivery__subtitle">
            Осуществляет доставку товаров по городу Ташкент. Доставка
            производится в течение 48 часов с момента подтверждения заказа
            покупателем.
          </p>
        </div>

        <ul className="delivery__list">
          <li className="delivery__item">
            <DeliveryItem />
          </li>
          <li className="delivery__item">
            <DeliveryItem />
          </li>
          <li className="delivery__item">
            <DeliveryItem />
          </li>
          <li className="delivery__item">
            <DeliveryItem />
          </li>
          <li className="delivery__item">
            <DeliveryItem />
          </li>
          <li className="delivery__item">
            <DeliveryItem />
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Delivery;
