import React, { useEffect, useState } from "react";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import Navbar from "../../components/Navbar/Navbar";
import Topbar from "../../components/Topbar/Topbar";
import "./formalize.css";

const Formalize = ({ data }) => {
  let [totalSum, setTotal] = useState(0);

  useEffect(() => {
    let counter = 0;
    if (data.length > 0) {
      for (let item of data) {
        counter += item.price * item.quantity;
        setTotal(counter);
      }
    } else {
      setTotal(0);
    }
  }, []);

  return (
    <div className="formalize">
      <div className="container formalize__container">
        <form
          className="formalize__form"
          action="https://echo.htmlacademy.ru"
          method="Post"
        >
          <h2 className="formalize__headline">Оформление заказа</h2>
          <label className="formalize__label">
            <span className="formalize__label-name">Ф.И.О.</span>
            <input
              className="formalize__input"
              type="text"
              name="full_name"
              placeholder="Напишите полное имя"
            />
          </label>

          <label className="formalize__label">
            <span className="formalize__label-name">Номер телефона</span>
            <input
              className="formalize__input"
              type="text"
              name="phone_num"
              defaultValue={"+998"}
              maxLength={13}
            />
          </label>

          <label className="formalize__label">
            <span className="formalize__label-name">Регион</span>
            <select
              className="formalize__input formalize__select"
              placeholder="Выберите ваш регион"
              name="region"
            >
              <option selected disabled style={{ color: "#9692B0 !important" }}>
                Выберите ваш регион
              </option>
              <option value="tashkent">Tashkent</option>
              <option value="samarqand">Samarqand</option>
              <option value="buxoro">Buxoro</option>
            </select>
          </label>

          <label className="formalize__label">
            <span className="formalize__label-name">
              Выберите своё местоположение на ка рте
            </span>

            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d749.0902788126746!2d69.19092564963823!3d41.3227602781908!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38ae8dbb8e1f0391%3A0xd04770323fe890a5!2sUdevs!5e0!3m2!1suz!2s!4v1651016983557!5m2!1suz!2s"
              width="519"
              height="209"
              style={{ border: 0 }}
              allowFullScreen=""
              loading="lazy"
              referrerPolicy="no-referrer-when-downgrade"
            ></iframe>
          </label>

          <div className="formalize__label">
            <span className="formalize__label-name">
              Выберите способ оплаты
            </span>

            <div className="formalize__labels-wrapper">
              <label className="formalize__inner-label">
                <input
                  className="formalize__radio visually-hidden"
                  type="radio"
                  name="payment-type"
                  aria-label="payme"
                  value={"payme"}
                />
                <span className="formalize__custom-input payme"></span>
              </label>

              <label className="formalize__inner-label">
                <input
                  className="formalize__radio visually-hidden"
                  type="radio"
                  name="payment-type"
                  value={"humo"}
                />
                <span className="formalize__custom-input humo"></span>
              </label>

              <label className="formalize__inner-label">
                <input
                  className="formalize__radio visually-hidden"
                  type="radio"
                  name="payment-type"
                  value={"uzcard"}
                />

                <span className="formalize__custom-input uzcard"></span>
              </label>
            </div>
          </div>

          <div className="formalize__label">
            <span className="formalize__label-name">
              Выберите способ доставки
            </span>

            <div className="formalize__delivery-labels-wrapper">
              <label className="formalize__delivery-label">
                <input
                  className="formalize__delivery-radio visually-hidden"
                  type="radio"
                  name="delivery-type"
                  value={"delivery"}
                />
                <span className="formalize__delivery-custom-input">
                  Доставка в течении дня
                </span>
              </label>

              <label className="formalize__delivery-label">
                <input
                  className="formalize__delivery-radio visually-hidden"
                  type="radio"
                  name="delivery-type"
                  value={"pickup"}
                />
                <span className="formalize__delivery-custom-input pickup">
                  Самовывоз
                </span>
              </label>
            </div>
          </div>

          <label className="formalize__label">
            <span className="formalize__label-name">Примечания к заказу</span>

            <textarea
              className="formalize__input formalize__textarea"
              name="addition"
              placeholder="Заметки о вашем заказе, например заметки для доставки"
            ></textarea>
          </label>

          <p className="formalize__txt">
            Совершая эту покупку, вы соглашаетесь с нашими{" "}
            <a className="formalize__link" href="#">
              условиями
            </a>
          </p>

          <button className="formalize__submit-btn" type="submit">
            Оформить заказ
          </button>
        </form>

        <div className="formalize__order">
          <h3 className="formalize__order-headline">Ваш заказ</h3>
          <dl className="formalize__order-def-list">
            {data.map((item) => (
              <div className="formalize__order-def-inner">
                <dt className="formalize__order-def-title">
                  <span className="formalize__order-quantity">
                    {item.quantity} x
                  </span>
                  {item.name}
                </dt>

                <dd className="formalize__order-def-desc">
                  ${item.price * item.quantity}
                </dd>
              </div>
            ))}

            {/* <div className="formalize__order-def-inner">
              <dt className="formalize__order-def-title">
                <span className="formalize__order-quantity">1 x</span>
                Samsung Galaxy A41 Red 128 GB
              </dt>

              <dd className="formalize__order-def-desc">
                $340
              </dd>
            </div> */}

            {/* <div className="formalize__order-def-inner">
              <dt className="formalize__order-def-title">
                <span className="formalize__order-quantity">1 x</span>
                Samsung Galaxy A41 Red 256 GB
              </dt>

              <dd className="formalize__order-def-desc">
                $400
              </dd>
            </div> */}
          </dl>

          <div className="formalize__order-delivery">
            <span className="formalize__order-delivery-txt">
              Сумма доставки
            </span>
            <span className="formalize__order-delivery-price">Бесплатно</span>
          </div>

          <p className="formalize__total">
            Итого <span className="formalize__total-price">${totalSum}</span>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Formalize;
