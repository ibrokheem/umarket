import React from "react";
import "./shops.css";
import Topbar from "../../components/Topbar/Topbar";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import ShopItem from "../../components/ShopItem/ShopItem";
import Navbar from "../../components/Navbar/Navbar";
import GoodzoneImg from "../../Assets/images/goodzone.png";

const Shops = () => {
  return (
    <div className="shops">
      <div className="container shops__container">
        <h2 className="shops__headline">Магазины</h2>

        <ul className="shops__list">
          <li className="shops__item">
            <ShopItem></ShopItem>
          </li>

          <li className="shops__item">
            <ShopItem></ShopItem>
          </li>

          <li className="shops__item">
            <ShopItem></ShopItem>
          </li>

          <li className="shops__item">
            <ShopItem></ShopItem>
          </li>

          <li className="shops__item">
            <ShopItem></ShopItem>
          </li>
        </ul>

        <div className="shops__how-to-reach">
          <div className="shops__how-to-reach-left">
            <img
              className="shops__how-to-reach__img"
              src={GoodzoneImg}
              alt="goodzone"
              width={411}
              height={130}
            />

            <ul className="shops__how-to-reach__list">
              <li className="shops__how-to-reach__item">
                <address className="shops__how-to-reach__address">
                  г. Ташкент, Сергелийский <br /> р-н, Янги Сергели
                </address>
              </li>

              <li className="shops__how-to-reach__item">
                <address className="shops__how-to-reach__address">
                  Напротив входа в машинный базар
                </address>
              </li>

              <li className="shops__how-to-reach__item">
                <time className="shops__how-to-reach__time">
                  с 10:00 до 21:00 (без выходных)
                </time>
              </li>

              <li className="shops__how-to-reach__item">
                <p className="shops__how-to-reach__tel">71−207−03−07</p>
              </li>
            </ul>
          </div>

          <div className="shops__how-to-reach-right">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d749.0902788126746!2d69.19092564963823!3d41.3227602781908!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38ae8dbb8e1f0391%3A0xd04770323fe890a5!2sUdevs!5e0!3m2!1suz!2s!4v1651016983557!5m2!1suz!2s"
              width="853"
              height="393"
              style={{ border: 0 }}
              allowFullScreen=""
              loading="lazy"
              referrerPolicy="no-referrer-when-downgrade"
            ></iframe>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Shops;
