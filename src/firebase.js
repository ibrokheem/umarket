// import firebase from "firebase/app"
// import "firebase/auth"
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const app = firebase.initializeApp({
  // apiKey: process.env.REACT__APP_API_KEY,
  // authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  // projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  // storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  // messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  // appId: process.env.REACT_APP_FIREBASE_APP_ID

  apiKey: "AIzaSyDn_DwPTezd4VZ_KVLgjeFt8viw_9qepe8",
  authDomain: "umarket-auth.firebaseapp.com",
  projectId: "umarket-auth",
  storageBucket: "umarket-auth.appspot.com",
  messagingSenderId: "500236399291",
  appId: "1:500236399291:web:7590e17a0e60d98b952f9e"
})

export const auth = app.auth()
export default app
