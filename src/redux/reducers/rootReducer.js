import { combineReducers } from "redux";
import passProductInfo from "./passProductInfo"
import login from "./login"

const rootReducer = combineReducers({
    // Add reducers here
    passProductInfo,
    login,
});

export default rootReducer;