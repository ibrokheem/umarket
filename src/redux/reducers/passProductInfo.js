const initialState = {
  name: "",
  img: "",
  rating: "",
  price: "",
  color: "",
  memory: "",
  sim: "",
  comments: "",
  available: true,
};

const productInfoReducer = (state = initialState, action) => {
  switch (action.type) {
    case "PRODUCT-INFO":
      return {
        ...action.payload,
      };
    default:
      return state;
  }
};

export default productInfoReducer;
