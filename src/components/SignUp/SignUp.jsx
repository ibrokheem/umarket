import React, {useRef, useState} from "react";
import "../../screens/Login/login.css";
import "./sign-up.css";
import { Link } from "react-router-dom";
import SweetAlert from "react-bootstrap-sweetalert";
// import { useAuth } from "../../contexts/AuthContext";

const SignUp = ({ show, setShowLoginModal, setIsRegistered }) => {
  const nameRef = useRef();
  const surnameRef = useRef();
  const telRef = useRef();
  const passwordRef = useRef();
  const confirmPasswordRef = useRef();

  // const { signup } = useAuth();

  const [error, setError] = useState("")

  // async function handleFormSubmit(e) {
  //   e.preventDefault();

  //   if (passwordRef.current.value !== confirmPasswordRef.current.value) {
  //     return setError("Passwords do not match")
  //   }

  //   try {
  //     setError("")
  //     await signup(telRef.current.value, passwordRef.current.value)
  //   } catch {
  //     setError("Failed")
  //   }
  // }

  return (
    <SweetAlert
      show={show}
      confirmBtnStyle={{ display: "none" }}
      customClass="sign-up"
      title={""}
      onConfirm={() => console.log("")}
    >
      <button
        className="sign-up__btn-exit"
        type="button"
        onClick={() => setShowLoginModal(false)}
      ></button>

      <form
        className="login"
        // action="https://echo.htmlacademy.ru"
        // method="post"
        // onSubmit={handleFormSubmit}

      >
        <div className="login__container">
          <h2 className="login__headline">Добро пожаловать</h2>
          <input
            className="login__input"
            ref={nameRef}
            type="text"
            name="name"
            maxLength={14}
            placeholder={"Имя"}
          />
          <input
            className="login__input"
            ref={surnameRef}
            type="text"
            name="surname"
            maxLength={14}
            placeholder={"Фамилия"}
          />
          <input
            className="login__input"
            ref={telRef}
            type="tel"
            name="phone_number"
            maxLength={14}
            placeholder={"Номер телефона"}
          />
          <input
            className="login__input"
            ref={passwordRef}
            type="password"
            name="password"
            maxLength={14}
            placeholder={"Введите пароль"}
          />
          <input
            className="login__input"
            ref={confirmPasswordRef}
            type="password"
            name="confirm-password"
            maxLength={14}
            placeholder={"Подтверждение пароля"}
          />

          <button className="login__submit-btn" type="submit">
            Зарегистрироваться
          </button>

          <p className="login__txt">Или зарегистрироваться с</p>
          <div className="login__links-wrapper">
            <a className="login__login-link google-link" href="#"></a>
            <a className="login__login-link facebook-link" href="#"></a>
          </div>

          <p className="login__txt login__subtext">
            У вас есть аккаунт?
            <button className="login__link" type="button" onClick={() => {setIsRegistered(true)}}>
              Войти
            </button>

            {/* <button className="login__link" type="button" onClick={myFunc}>
              Войти
            </button> */}
          </p>
        </div>
      </form>
    </SweetAlert>
  );
};

export default SignUp;
