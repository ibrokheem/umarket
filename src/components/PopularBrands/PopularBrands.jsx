import React from "react";
import "./popular-brands.css";
import NokiaImg from "../../Assets/images/nokia.svg";
import AppleImg from "../../Assets/images/apple.svg";
import SamsungImg from "../../Assets/images/samsung.svg";
import HuaweiImg from "../../Assets/images/huawei.svg";
import LgImg from "../../Assets/images/lg.svg";
import Xiaomi from "../../Assets/images/xiaomi.svg";
import Avatar from "../../Assets/images/avatar.png";
import { Tabs, TabLink, TabContent } from "react-tabs-redux";

const PopularBrands = ({ title }) => {
  return (
    <section className="popular-brands">
      <div className="container popular-brands__container">
        <h2 className="popular-brands__headline">{title}</h2>

        <Tabs className="popular-brands__categories-list">
          <TabLink className="popular-brands__categories-item" to="tab1">
            Телефоны
          </TabLink>

          <TabLink className="popular-brands__categories-item" to="tab2">
            Аксессуары
          </TabLink>

          <TabLink className="popular-brands__categories-item" to="tab3">
            Premium
          </TabLink>

          <TabLink className="popular-brands__categories-item" to="tab4">
            Спорт
          </TabLink>

          <TabLink className="popular-brands__categories-item" to="tab5">
            Игрушки
          </TabLink>

          <TabLink className="popular-brands__categories-item" to="tab6">
            Красота
          </TabLink>

          <TabLink className="popular-brands__categories-item" to="tab7">
            Книги
          </TabLink>

          <TabLink className="popular-brands__categories-item" to="tab8">
            Обувь
          </TabLink>

          <TabContent className="popular-brands__tabcontent" for="tab1">
            <ul className="popular-brands__brands-list">
              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={NokiaImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={AppleImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={HuaweiImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={LgImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>
            </ul>
          </TabContent>

          <TabContent className="popular-brands__tabcontent" for="tab2">
            <ul className="popular-brands__brands-list">
              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={LgImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={NokiaImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={AppleImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={HuaweiImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>
            </ul>
          </TabContent>

          <TabContent className="popular-brands__tabcontent" for="tab3">
            <ul className="popular-brands__brands-list">
              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={AppleImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={NokiaImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={HuaweiImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={LgImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>
            </ul>
          </TabContent>

          <TabContent className="popular-brands__tabcontent" for="tab4">
            <ul className="popular-brands__brands-list">
              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={NokiaImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={AppleImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={HuaweiImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={LgImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>
            </ul>
          </TabContent>

          <TabContent className="popular-brands__tabcontent" for="tab5">
            <ul className="popular-brands__brands-list">
              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={AppleImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={NokiaImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={HuaweiImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={LgImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>
            </ul>
          </TabContent>

          <TabContent className="popular-brands__tabcontent" for="tab6">
            <ul className="popular-brands__brands-list">
              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>
              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={NokiaImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={AppleImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={HuaweiImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={LgImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>
            </ul>
          </TabContent>

          <TabContent className="popular-brands__tabcontent" for="tab7">
            <ul className="popular-brands__brands-list">
              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={HuaweiImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={LgImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={NokiaImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={AppleImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              
            </ul>
          </TabContent>

          <TabContent className="popular-brands__tabcontent" for="tab8">
            <ul className="popular-brands__brands-list">
              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={NokiaImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={AppleImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={LgImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={SamsungImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={HuaweiImg}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>

              <li className="popular-brands__brands-item">
                <img
                  className="popular-brands__brands-img"
                  src={Xiaomi}
                  alt="nokia"
                  width={117}
                  height={51}
                />
              </li>
            </ul>
          </TabContent>
        </Tabs>
      </div>
    </section>
  );
};

export default PopularBrands;
