import React from "react";
import "./navbar.css"

const Navbar = () => {
  return (
    <nav className="navbar">
      <div className="container navbar__container">
        <ol className="navbar__list">
          <li className="navbar__item">
            <a className="navbar__link" href="#">
              Акции и скидки
            </a>
          </li>

          <li className="navbar__item">
            <a className="navbar__link" href="#">
              Смартфоны и гаджеты
            </a>
          </li>

          <li className="navbar__item">
            <a className="navbar__link" href="#">
              Телевизоры и аудио
            </a>
          </li>

          <li className="navbar__item">
            <a className="navbar__link" href="#">
              Техника для кухни
            </a>
          </li>

          <li className="navbar__item">
            <a className="navbar__link" href="#">
              Красота и здоровье
            </a>
          </li>

          <li className="navbar__item">
            <a className="navbar__link" href="#">
              Ноутбуки и компьютеры
            </a>
          </li>
        </ol>
      </div>
    </nav>
  );
};

export default Navbar;
