import React from "react";
import { useState } from "react";
import { loginAction } from "../../redux/actions/loginAction";
import Login from "../../screens/Login/Login";
import { BrowserRouter, Link, Route, Routes, Switch } from "react-router-dom";
import "./header.css";
import Logo from "../../Assets/images/main-logo.svg";
import { useDispatch, useSelector } from "react-redux";

import {
  LocationIcon,
  LoginIcon,
  SearchIcon,
  ShoppingCartIcon,
  HeartIcon,
  CompareIcon,
} from "../../Assets/icons/icons.js";
import SignIn from "../SignIn/SignIn";
import Carousel from "react-multi-carousel";

const Header = ({ cart, salesHits, setSalesHits }) => {
  const [isRegistered, setIsRegistered] = useState(true);
  const [showLoginModal, setShowLoginModal] = useState(false);
  const [hamburger, setHamburger] = useState(false);
  const [inputModal, setInputModal] = useState(false);

  function onLoginClick(evt) {
    setShowLoginModal(!showLoginModal);
  }

  return (
    <>
      <header className="header">
        <div className="container header__container">
          <Link className="main-logo" to="/">
            <img src={Logo} alt="umarket" width={152} height={52} />
          </Link>

          <form
            className={`header__form ${
              inputModal ? "header__form--active" : ""
            }`}
            action="https://echo.htmlacademy.ru"
            method="Post"
          >
            <input
              className="header__search-input"
              name="search"
              type="search"
              aria-label="search"
              placeholder="Поиск по товарам"
            />

            <div className="header__search-by-photo">
              <a className="header__search-by-photo-link" href="#">
                <svg
                  width="25"
                  height="24"
                  viewBox="0 0 25 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M21.4517 3H3.45166C2.45166 3 1.45166 4 1.45166 5V19C1.45166 20.1 2.35166 21 3.45166 21H21.4517C22.4517 21 23.4517 20 23.4517 19V5C23.4517 4 22.4517 3 21.4517 3ZM21.4517 18.92C21.4317 18.95 21.3917 18.98 21.3717 19H3.45166V5.08L3.53166 5H21.3617C21.3917 5.02 21.4217 5.06 21.4417 5.08V18.92H21.4517ZM11.4517 15.51L8.95166 12.5L5.45166 17H19.4517L14.9517 11L11.4517 15.51Z"
                    fill="currentColor"
                  />
                </svg>
              </a>
              <span className="tooltip">Поиск по фото</span>
            </div>

            <button
              className="header__cancel-search-btn"
              type="button"
              onClick={() => setInputModal(!inputModal)}
            ></button>
          </form>

          <button
            className="header__search-btn"
            type="button"
            onClick={() => setInputModal(!inputModal)}
          ></button>

          <div className="header__controls-wrapper">
            <ul className="header__controls">
              <li className="header__controls-item">
                <Link className="header-controls-link" to="/cart">
                  <ShoppingCartIcon />
                  <span className="header-controls-link-txt">Корзина</span>
                  <span className="header-controls__counter">
                    {cart.length}
                  </span>
                </Link>
              </li>
              <li className="header__controls-item">
                <Link className="header-controls-link" to="/saved">
                  <HeartIcon />
                  <span className="header-controls-link-txt">Избранные</span>
                  <span className="header-controls__counter">5</span>
                </Link>
              </li>
              <li className="header__controls-item">
                <a className="header-controls-link" href="#">
                  <CompareIcon />
                  <span className="header-controls-link-txt">Сравнение</span>
                  <span className="header-controls__counter">6</span>
                </a>
              </li>
              <li className="header__controls-item" onClick={onLoginClick}>
                <a className="header-controls-link" href="#">
                  <LoginIcon />
                  <span className="header-controls-link-txt">Войти</span>
                </a>
              </li>
            </ul>
          </div>

          <div
            className={`menu hamburger ${hamburger ? "open" : ""}`}
            onClick={() => setHamburger(!hamburger)}
          >
            <div className="icon"></div>
          </div>
        </div>
      </header>

      <Login
        isRegistered={isRegistered}
        setIsRegistered={setIsRegistered}
        show={showLoginModal}
        setShowLoginModal={setShowLoginModal}
      ></Login>
    </>
  );
};

export default Header;
