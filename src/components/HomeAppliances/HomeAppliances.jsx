import React from "react";
import "./home-appliances.css";

const HomeAppliances = ({ title, data }) => {
  return (
    <section className="home-appliances">
      <div className="container home-appliances__container">
        <h2 className="home-appliances__headline">{title}</h2>

        <ul className="home-appliances__list">
          {data.map((item, i) => (
            <li className="home-appliances__item" key={i}>
              <img
                className="home-appliances__item-img"
                src={item.img}
                alt="smartphones"
                width={267}
                height={160}
              />
              <a href="#" className="home-appliances__item-name">
                {item.title}
              </a>
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
};

export default HomeAppliances;
