import React from "react";
import "./product-modal.css";
import SweetAlert from "react-bootstrap-sweetalert";
import { Link } from "react-router-dom";

const ProductModal = ({ show, product, setShowProductModal }) => {
  return (
    <SweetAlert
      show={show}
      confirmBtnStyle={{ display: "none" }}
      customClass="product-modal"
      title={""}
      onConfirm={() => console.log("")}
    >
      <div className="product-modal__header">
        <div className="product-modal__top">
          <h3 className="product-modal__name">{product.name}</h3>
          <button className="product-modal__btn-exit" type="button" onClick={()=> setShowProductModal(false)}></button>
        </div>

        <div className="product-modal__header-inner">
          <div className="products-item__rating-wrapper product-modal__rating-wrapper">
            <span className="products-item__rating product-modal__rating"></span>
            <span className="products-item__rating product-modal__rating"></span>
            <span className="products-item__rating product-modal__rating"></span>
            <span className="products-item__rating product-modal__rating"></span>
            <span className="products-item__rating product-modal__rating"></span>
          </div>
          <p className="products-modal__comments-quantity">
            {product.comments} отзывов
          </p>
          <span className="products-modal__availability">
            {product.available ? "Есть в наличии" : "Нет в наличии"}
          </span>
        </div>
      </div>

      <div className="products-modal__content">
        <div className="products-modal__photos-wrapper">
          <div className="products-modal__main-photo-wrapper">
            <img
              className="products-modal__main-photo"
              src={product.img}
              alt="red samsung galaxy a41"
              width={296}
              height={295}
            />
          </div>

          <div className="products-modal__additional-photos-wrapper">
            <div className="products-modal__additional-photo-wrapper">
              <img
                className="products-modal__additional-photo"
                src={product.img}
                alt="red samsung galaxy a41"
                width={86}
                height={86}
              />
            </div>

            <div className="products-modal__additional-photo-wrapper">
              <img
                className="products-modal__additional-photo"
                src={product.img}
                alt="red samsung galaxy a41"
                width={86}
                height={86}
              />
            </div>

            <div className="products-modal__additional-photo-wrapper">
              <img
                className="products-modal__additional-photo"
                src={product.img}
                alt="red samsung galaxy a41"
                width={86}
                height={86}
              />
            </div>

            <div className="products-modal__additional-photo-wrapper">
              <img
                className="products-modal__additional-photo"
                src={product.img}
                alt="red samsung galaxy a41"
                width={86}
                height={86}
              />
            </div>
          </div>
        </div>

        <form
          className="products-modal__properties"
          // action="https://echo.htmlacademy.ru"
          // method="Post"
        >
          <p className="products-modal__price">{product.price}</p>

          <div className="products-modal__colors-list">
            <p className="products-modal__txt">Цвет</p>

            <div className="products-modal__labels-wrapper">
              <label className="products-modal__colors-label">
                <input
                  className="products-modal__colors-input visually-hidden"
                  type="radio"
                  name="color"
                  value="red"
                />
                <span className="products-modal__colors-custom-input tomato"></span>
              </label>

              <label className="products-modal__colors-label">
                <input
                  className="products-modal__colors-input visually-hidden"
                  type="radio"
                  name="color"
                  value="grey"
                />
                <span className="products-modal__colors-custom-input grey"></span>
              </label>

              <label className="products-modal__colors-label">
                <input
                  className="products-modal__colors-input visually-hidden"
                  type="radio"
                  name="color"
                  value="blue"
                />
                <span className="products-modal__colors-custom-input blue"></span>
              </label>
            </div>
          </div>

          <div className="products-modal__memories-list">
            <p className="products-modal__txt">Память</p>

            <div className="products-modal__labels-wrapper">
              <label className="products-modal__memories-label">
                <input
                  className="products-modal__memories-input visually-hidden"
                  type="radio"
                  name="memory"
                  value="64"
                />
                <span className="products-modal__memories-custom-input">
                  64Gb
                </span>
              </label>

              <label className="products-modal__memories-label">
                <input
                  className="products-modal__memories-input visually-hidden"
                  type="radio"
                  name="memory"
                  value="128"
                />
                <span className="products-modal__memories-custom-input">
                  128GB
                </span>
              </label>

              <label className="products-modal__memories-label">
                <input
                  className="products-modal__memories-input visually-hidden"
                  type="radio"
                  name="memory"
                  value="256"
                />
                <span className="products-modal__memories-custom-input">
                  256GB
                </span>
              </label>
            </div>
          </div>

          <div className="products-modal__sim-list">
            <p className="products-modal__txt">SIM</p>
            <label className="products-modal__sim-label">
              <input
                className="products-modal__sim-input visually-hidden"
                type="radio"
                name="sim"
                value="dual"
              />
              <span className="products-modal__sim-custom-input">Dual</span>
            </label>

            <label className="products-modal__sim-label">
              <input
                className="products-modal__sim-input visually-hidden"
                type="radio"
                name="sim"
                value="single"
              />
              <span className="products-modal__sim-custom-input">Single</span>
            </label>
          </div>

          <button className="products-modal__add-cart-btn" type="submit">
            Добавить в корзину
          </button>
        </form>
      </div>

      <Link to={"/product-page"} className="products-modal__more-info-btn">
        Подробно о товаре
      </Link>
    </SweetAlert>
  );
};

export default ProductModal;
