import React from 'react';
import "./shop-item.css"

const ShopItem = () => {
  return (
    <div className='shop-item'>
      <p className='shop-item__name'>Goodzone Сергели</p>
      <address className='shop-item__address'>г. Ташкент, Сергелийский район, Янги Сергели <span className='shop-item__address-inner'>Ориентир: Напротив входа в машинный базар</span></address>
      <p className='shop-item__work-hours'>Часы работы: с <time dateTime='10:00'>10:00</time> до <time dateTime='21:00'>21:00</time> (без выходных)</p>
    </div>
  );
};

export default ShopItem;