import React from "react";
import { Link } from "react-router-dom";
import "./footer.css";
import PaymeImg from "../../Assets/images/payme.svg";
import HumoImg from "../../Assets/images/humo.svg";
import UzcardImg from "../../Assets/images/uzcard.svg";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="container footer-container">
        <div className="footer__top">
          <div className="footer__nav">
            <p className="footer__nav-name">Компания</p>
            <ul className="footer__nav-list">
              <li className="footer__nav-item">
                <a className="footer__nav-link" href="#">
                  О компании
                </a>
              </li>

              <li className="footer__nav-item">
                <Link className="footer__nav-link" to="/shops">
                  Адреса магазинов
                </Link>
              </li>
            </ul>
          </div>

          <div className="footer__nav">
            <p className="footer__nav-name">Информация</p>
            <ul className="footer__nav-list">
              <li className="footer__nav-item">
                <a className="footer__nav-link" href="#">
                  Рассрочка
                </a>
              </li>

              <li className="footer__nav-item">
                <Link className="footer__nav-link" to="/delivery">
                  Доставка
                </Link>
              </li>

              <li className="footer__nav-item">
                <a className="footer__nav-link" href="#">
                  Бонусы
                </a>
              </li>
            </ul>
          </div>

          <div className="footer__nav">
            <p className="footer__nav-name">Помощь покупателю</p>
            <ul className="footer__nav-list">
              <li className="footer__nav-item">
                <Link className="footer__nav-link" to=
                "/review">
                  Вопросы и ответы
                </Link>
              </li>

              <li className="footer__nav-item">
                <a className="footer__nav-link" href="#">
                  Как сделать заказ на сайте
                </a>
              </li>

              <li className="footer__nav-item">
                <a className="footer__nav-link" href="#">
                  Обмен и возврат
                </a>
              </li>
            </ul>
          </div>

          <div className="footer__nav">
            <p className="footer__nav-name">Способ оплаты</p>
            <ul className="footer__payment-list">
              <li className="footer__payment-item">
                <a href="#">
                  <img src={PaymeImg} alt="payme" width={81} height={23} />
                </a>
              </li>

              <li className="footer__payment-item">
                <a href="#">
                  <img src={HumoImg} alt="humo" width={81} height={23} />
                </a>
              </li>

              <li className="footer__payment-item uzcard">
                <a href="#">
                  <img src={UzcardImg} alt="humo" width={22} height={30} />
                </a>
              </li>
            </ul>
          </div>

          <div className="footer__socials">
            <p className="footer__nav-name">Мы в социальных сетях</p>
            <ul className="footer__socials-list">
              <li className="footer__socials-item">
                <a className="footer__socials-link" href="#">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M16 8C17.5913 8 19.1174 8.63214 20.2426 9.75736C21.3679 10.8826 22 12.4087 22 14V21H18V14C18 13.4696 17.7893 12.9609 17.4142 12.5858C17.0391 12.2107 16.5304 12 16 12C15.4696 12 14.9609 12.2107 14.5858 12.5858C14.2107 12.9609 14 13.4696 14 14V21H10V14C10 12.4087 10.6321 10.8826 11.7574 9.75736C12.8826 8.63214 14.4087 8 16 8V8Z"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M6 9H2V21H6V9Z"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M4 6C5.10457 6 6 5.10457 6 4C6 2.89543 5.10457 2 4 2C2.89543 2 2 2.89543 2 4C2 5.10457 2.89543 6 4 6Z"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </a>
              </li>

              <li className="footer__socials-item">
                <a className="footer__socials-link" href="#">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M17 2H7C4.23858 2 2 4.23858 2 7V17C2 19.7614 4.23858 22 7 22H17C19.7614 22 22 19.7614 22 17V7C22 4.23858 19.7614 2 17 2Z"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M16 11.3701C16.1234 12.2023 15.9812 13.0523 15.5937 13.7991C15.2062 14.5459 14.5931 15.1515 13.8416 15.5297C13.0901 15.908 12.2384 16.0397 11.4077 15.906C10.5771 15.7723 9.80971 15.3801 9.21479 14.7852C8.61987 14.1903 8.22768 13.4229 8.09402 12.5923C7.96035 11.7616 8.09202 10.91 8.47028 10.1584C8.84854 9.40691 9.45414 8.7938 10.2009 8.4063C10.9477 8.0188 11.7977 7.87665 12.63 8.00006C13.4789 8.12594 14.2648 8.52152 14.8716 9.12836C15.4785 9.73521 15.8741 10.5211 16 11.3701Z"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M17.5 6.5H17.51"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </a>
              </li>

              <li className="footer__socials-item">
                <a className="footer__socials-link" href="#">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M23 3.00005C22.0424 3.67552 20.9821 4.19216 19.86 4.53005C19.2577 3.83756 18.4573 3.34674 17.567 3.12397C16.6767 2.90121 15.7395 2.95724 14.8821 3.2845C14.0247 3.61176 13.2884 4.19445 12.773 4.95376C12.2575 5.71308 11.9877 6.61238 12 7.53005V8.53005C10.2426 8.57561 8.50127 8.18586 6.93101 7.39549C5.36074 6.60513 4.01032 5.43868 3 4.00005C3 4.00005 -1 13 8 17C5.94053 18.398 3.48716 19.099 1 19C10 24 21 19 21 7.50005C20.9991 7.2215 20.9723 6.94364 20.92 6.67005C21.9406 5.66354 22.6608 4.39276 23 3.00005V3.00005Z"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </a>
              </li>

              <li className="footer__socials-item">
                <a className="footer__socials-link" href="#">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M18 2H15C13.6739 2 12.4021 2.52678 11.4645 3.46447C10.5268 4.40215 10 5.67392 10 7V10H7V14H10V22H14V14H17L18 10H14V7C14 6.73478 14.1054 6.48043 14.2929 6.29289C14.4804 6.10536 14.7348 6 15 6H18V2Z"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </a>
              </li>
            </ul>
          </div>
        </div>

        <div className="footer__bottom">
          <div className="footer__nav footer__bottom-nav">
            <p className="footer__nav-name">Единый кол центр</p>
            <a className="footer__nav-link" href="tel:+9980 71546060">
              +9980 71-54-60-60
            </a>
          </div>

          <div className="footer__nav footer__bottom-nav">
            <p className="footer__nav-name">
              Почта для пожеланий и предложений
            </p>
            <a className="footer__nav-link" href="mailto:info@udevsmarket.com">
              info@udevsmarket.com
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
