import React from "react";
import ProductsItem from "../ProductsItem/ProductsItem";
import "./products-list.css";

const ProductsList = ({ title, products, setProducts, cart, setCart }) => {
  return (
    <section className="products-list">
      <div className="container products-list__container">
        <h2 className="products-list__headline">{title}</h2>

        <ul className="products-list__list">
          {products?.map((product) => (
            <li className="products-list__item" key={product.id}>
              <ProductsItem
                data={product}
                products={products}
                setProducts={setProducts}
                cart={cart}
                setCart={setCart}
              />
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
};

export default ProductsList;
