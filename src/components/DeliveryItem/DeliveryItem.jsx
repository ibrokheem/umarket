import React from "react";
import "./delivery-item.css"

const DeliveryItem = () => {
  return (
    <div className="delivery-item">
      <p className="delivery-item__txt">
        Доставка мелкой бытовой техники <br /> и электроники
      </p>
      <span className="delivery-item__price">30 000 сум</span>
    </div>
  );
};

export default DeliveryItem;
