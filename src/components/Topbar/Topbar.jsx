import { Link } from "react-router-dom";

import "./topbar.css";

const Topbar = () => {
  return (
    <div className="topbar">
      <div className="container topbar__container">
        <ul className="topbar__list">
          <li className="topbar__item">
            <Link className="topbar__link" to="/shops">
              Магазины
            </Link>
          </li>

          <li className="topbar__item">
            <Link className="topbar__link" to="/review">
              Оставить отзыв
            </Link>
          </li>

          <li className="topbar__item">
            <Link className="topbar__link" to="/delivery">
              Доставка
            </Link>
          </li>
        </ul>

        <div className="topbar-right">
          <a className="topbar-right-tel" href="tel:+998977781708">
            +998 97 778-17-08
          </a>
          <address className="topbar__right-address">Ташкент</address>

          <button className="topbar__right-language-picker" type="button">
            <span className="topbar__right-language-picker-txt">Рус</span>
          </button>
        </div>
      </div>
    </div>
  );
};

export default Topbar;
