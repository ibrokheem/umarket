import React from "react";
import "./categories.css";

const Categories = ({ title, data }) => {
  return (
    <section className="categories">
      <div className="container categories__container">
        <h2 className="categories__headline">{title}</h2>

        <ul className="categories__list">
          {data.map((item, i) => (
            <li className="categories__item" key={i}>
              <img
                className="categories__item-img"
                src={item.img}
                alt="smartphones"
                width={267}
                height={160}
              />
              <a href="#" className="categories__item-name">
                {item.title}
              </a>
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
};

export default Categories;
