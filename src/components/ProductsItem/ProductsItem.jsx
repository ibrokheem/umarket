import React, { useState, useEffect } from "react";
import "./products-item.css";
import PhoneImg from "../../Assets/images/samsun-a-41.png";
import { useDispatch, useSelector } from "react-redux";
import { passProductInfo } from "../../redux/actions/passProductInfo";
import ProductModal from "../ProductModal/ProductModal";

const ProductsItem = ({ data, products, setProducts, cart, setCart }) => {
  if (cart.some((item) => item.id == data.id)) {
    data.inCart = true;
  } else {
    data.inCart = false;
    data.quantity = 1;
  }
  const [counter, setCounter] = useState(data.quantity);

  data.quantity = counter;

  useEffect(() => {
    setProducts(products);
    setCart(cart)
    localStorage.setItem("cart", JSON.stringify(cart));
    localStorage.setItem("salesHits", JSON.stringify(products));
  }, [counter]);

  const dispatch = useDispatch();
  const [showProductModal, setShowProductModal] = useState(false);
  const productInfo = useSelector((state) => state.passProductInfo);

  function onProductClick(evt) {
    dispatch(
      passProductInfo(
        ...products.filter((item) => item.id == evt.target.dataset.id)
      )
    );
    setShowProductModal(!showProductModal);
  }

  function addCart(evt) {
    let newCartElement = products.find(
      (element) => element.id == evt.target.dataset.id
    );

    setCart([...cart, newCartElement]);
    // setCart([{title: 'molakansan'}]);
    localStorage.setItem("cart", JSON.stringify(cart));
    setProducts(products);
    localStorage.setItem("salesHits", JSON.stringify(products));
    console.log('cart', cart);
  }

  return (
    <div className="products-item">
      <div className="wrapper">
        <img
          className="products-item__img"
          src={data.img}
          alt="samsung a41 red"
          width={252}
          height={179}
        />
        <button
          className="products-item__show-item"
          type="button"
          onClick={onProductClick}
          data-id={data.id}
        >
          Быстрый просмотр
        </button>
      </div>

      <div className="products-item__content">
        <h3 className="products-item__name">
          {data.name} <br></br> {data.memory}
        </h3>

        <span className="products-item__price">{data.priceUZS}</span>
        <span className="products-item__credit">От {data.credit}/12 мес</span>

        <div className="products-item__rating-wrapper">
          <span className="products-item__rating"></span>
          <span className="products-item__rating"></span>
          <span className="products-item__rating"></span>
          <span className="products-item__rating"></span>
          <span className="products-item__rating"></span>
        </div>

        <div className="products-item__controls">
          {data.inCart ? (
            <div>
              <button
                type="button"
                onClick={() => setCounter(counter > 1 ? counter - 1 : counter)}
              >
                -
              </button>
              <span>{data.quantity}</span>
              <button type="button" onClick={() => setCounter(counter + 1)}>
                +
              </button>
            </div>
          ) : (
            <button
              className="products-item__btn-add-cart"
              type="button"
              onClick={addCart}
              data-id={data.id}
            >
              <svg
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M12.9585 10.8334C13.5835 10.8334 14.1335 10.4917 14.4168 9.97502L17.4002 4.56669C17.7085 4.01669 17.3085 3.33335 16.6752 3.33335H4.34183L3.5585 1.66669H0.833496V3.33335H2.50016L5.50016 9.65835L4.37516 11.6917C3.76683 12.8084 4.56683 14.1667 5.8335 14.1667H15.8335V12.5H5.8335L6.75016 10.8334H12.9585ZM5.1335 5.00002H15.2585L12.9585 9.16669H7.1085L5.1335 5.00002ZM5.8335 15C4.91683 15 4.17516 15.75 4.17516 16.6667C4.17516 17.5834 4.91683 18.3334 5.8335 18.3334C6.75016 18.3334 7.50016 17.5834 7.50016 16.6667C7.50016 15.75 6.75016 15 5.8335 15ZM14.1668 15C13.2502 15 12.5085 15.75 12.5085 16.6667C12.5085 17.5834 13.2502 18.3334 14.1668 18.3334C15.0835 18.3334 15.8335 17.5834 15.8335 16.6667C15.8335 15.75 15.0835 15 14.1668 15Z"
                  fill="white"
                />
              </svg>
              <span className="products-item__btn-add-cart-txt">В корзину</span>
            </button>
          )}

          {/* <button className="products-item__btn-order">Заказать</button> */}

          <div className="products-item__controls-inner">
            <button className="products-item__btn-compare" type="button">
              <svg
                width="21"
                height="20"
                viewBox="0 0 21 20"
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M18.785 6.66671L15.4517 3.33337V5.83337H2.95166V7.50004H15.4517V10L18.785 6.66671Z"
                  fill="currentColor"
                />
                <path
                  d="M2.11841 13.3333L5.45174 16.6667V14.1667H17.9517V12.5H5.45174V10L2.11841 13.3333Z"
                  fill="currentColor"
                />
              </svg>
            </button>

            <button className="products-item__btn-favourite" type="button">
              <svg
                width="21"
                height="20"
                viewBox="0 0 21 20"
                fill="#FFFFFF"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g clipPath="url(#clip0_305_3983)">
                  <path
                    d="M18.3183 3.84172C17.8926 3.41589 17.3873 3.0781 16.8311 2.84763C16.2748 2.61716 15.6787 2.49854 15.0766 2.49854C14.4745 2.49854 13.8784 2.61716 13.3221 2.84763C12.7659 3.0781 12.2606 3.41589 11.8349 3.84172L10.9516 4.72506L10.0683 3.84172C9.20852 2.98198 8.04246 2.49898 6.8266 2.49898C5.61074 2.49898 4.44467 2.98198 3.58493 3.84172C2.72519 4.70147 2.24219 5.86753 2.24219 7.08339C2.24219 8.29925 2.72519 9.46531 3.58493 10.3251L4.46826 11.2084L10.9516 17.6917L17.4349 11.2084L18.3183 10.3251C18.7441 9.89943 19.0819 9.39407 19.3124 8.83785C19.5428 8.28164 19.6615 7.68546 19.6615 7.08339C19.6615 6.48132 19.5428 5.88514 19.3124 5.32893C19.0819 4.77271 18.7441 4.26735 18.3183 3.84172V3.84172Z"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_305_3983">
                    <rect
                      width="20"
                      height="20"
                      fill="#FFFFFF"
                      transform="translate(0.95166)"
                    />
                  </clipPath>
                </defs>
              </svg>
            </button>
          </div>
        </div>
      </div>

      <ProductModal
        show={showProductModal}
        product={productInfo}
        setShowProductModal={setShowProductModal}
      />
    </div>
  );
};

export default ProductsItem;
