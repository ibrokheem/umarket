import React from "react";
import "./application-banner.css"
import GooglePlayImg from "../../Assets/images/play-market.png";
import AppStoreImg from "../../Assets/images/app-store.png";

const ApplicationBanner = () => {
  return (
    <section className="application-banner">
      <div className="container application-banner__container">
        <div className="application-banner__left">
          <p className="application-banner__name">mobile application</p>

          <h3 className="application__title">
            Заказывайте через мобильное приложение
          </h3>

          <div className="application-banner__app-stores">
            <a className="application-banner__google-play" href="#">
              <img
                className="application-banner__img"
                src={GooglePlayImg}
                alt="google play"
                width={168}
                height={50}
              />
            </a>

            <a className="application-banner__app-store" href="#">
              <img className="application-banner__img" src={AppStoreImg} alt="app store" width={142} height={50} />
            </a>
          </div>
        </div>

        <p className="application-banner__txt">Отсканируйте QR-код и установите приложение</p>
      </div>
    </section>
  );
};

export default ApplicationBanner;
