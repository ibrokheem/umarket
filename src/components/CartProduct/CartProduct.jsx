import { useState, useEffect, useRef } from "react";

const CartProduct = ( {cart, item, setCart, salesHits, setSalesHits, calculateTotal} ) => {
  const [count, setCount] = useState(item.quantity);
  item.quantity = count;
  const checkboxRef = useRef(false);

  useEffect(()=>{
    calculateTotal();

    let index = salesHits.findIndex(i => i.id == item.id)
    salesHits[index].quantity = item.quantity
    setSalesHits(salesHits);
    localStorage.setItem('cart', JSON.stringify(cart));
    localStorage.setItem('salesHits', JSON.stringify(salesHits));
  }, [count])  
  
  const handleChecboxChange = () => {
    item.isChecked = checkboxRef.current.checked;
  }

  const deleteItem = (id) => {
    setCart(cart.filter(item => item.id != id));
    // localStorage.setItem('cart', JSON.stringify(cart));
  }

  return (
    <li className="cart__item">
      <input
        className="cart__item-input"
        type="checkbox"
        name="item"
        value={"samsung a 41"}
        ref={checkboxRef}
        onChange={handleChecboxChange}
        defaultChecked={item.isChecked}
      />

      <img
        className="cart__item-img"
        src={item.img}
        alt="honor"
        width={105}
        height={128}
      />

      <div className="cart__item-content">
        <h3 className="cart__item-name">{item.name}</h3>

        <p className="cart__item-desc">
          Потрясающий экран, реальная плавная прокрутка
        </p>

        <div className="cart__btns-wrapper">
          <button
            className="cart__item__btn cart__item__btn--save"
            type="button"
          >
            В избранные
          </button>

          <button
            className="cart__item__btn cart__item__btn--delete"
            type="button"
            onClick={e => deleteItem(item.id)}
          >
            Удалить
          </button>
        </div>
      </div>

      <div className="cart__item-counter">
        <button
          className="cart__item-counter__btn"
          type="button"
          onClick={() => setCount(count > 1 ? count - 1 : count)}
        >
          -
        </button>

        <span className="cart__item-counter__quantity">{count}</span>

        <button
          className="cart__item-counter__btn"
          type="button"
          onClick={() => setCount(count + 1)}
        >
          +
        </button>

        <p className="cart__item-price">{item.price * count}$</p>
      </div>
    </li>
  );
};
export default CartProduct;
