import React from "react";
import "./banner.css";
import BannerImg from "../../Assets/images/banner.png";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import GoodzoneImg from "../../Assets/images/goodzone.png";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
    slidesToSlide: 1, // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
    slidesToSlide: 1, // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1, // optional, default to 1.
  },
};

const Banner = () => {
  return (
    <div className="banner">
      <div className="container banner__container">
        <Carousel
          swipeable={false}
          draggable={false}
          showDots={true}
          autoPlay={true}
          focusOnSelect={true}
          responsive={responsive}
          shouldResetAutoplay={true}
          ssr={true}
          infinite={true}
          autoPlaySpeed={1000}
          keyBoardControl={true}
          customTransition="all .5"
          transitionDuration={1000}
          containerClass="carousel-container"
          removeArrowOnDeviceType={["desktop","tablet", "mobile"]}
          dotListClass="custom-dot-list-style"
          itemClass="carousel-item-padding-40-px"
        >
          <div>
            <img className="banner__img" src={BannerImg} alt="haier" width={1296} height={346} />
          </div>
          <div>
            <img className="banner__img" src="https://picsum.photos/1296/346" alt="haier" width={1296} height={346} />
          </div>
          <div>
            <img className="banner__img" src={BannerImg} alt="haier" width={1296} height={346} />
          </div>
          <div>
            <img className="banner__img" src='https://picsum.photos/1296/346/?random' alt="haier" width={1296} height={346} />
          </div>
        </Carousel>
      </div>
    </div>
  );
};

export default Banner;
