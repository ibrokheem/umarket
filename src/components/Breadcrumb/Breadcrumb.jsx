import "./breadcrumb.css";

const Breadcrumb = () => {
  return (
    <div className="breadcrumb">
      <ul className="breadcrumb__list">
        <li className="breadcrumb__item">
          <a className="breadcrumb__link" href="#">
            Главная
          </a>
        </li>

        <li className="breadcrumb__item">
          <a className="breadcrumb__link" href="#">
            Телефоны
          </a>
        </li>

        <li className="breadcrumb__item">
          <a className="breadcrumb__link breadcrumb__link--active" href="#">
            Samsung Galaxy A41
          </a>
        </li>
      </ul>
    </div>
  );
};

export default Breadcrumb;
