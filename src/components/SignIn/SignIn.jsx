import React, {useRef} from "react";
import "../../screens/Login/login.css";
import "./sign-in.css";
import SweetAlert from "react-bootstrap-sweetalert";

const SignIn = ({ show, setShowLoginModal, setIsRegistered }) => {

  // const telRef = useRef();    

  return (
    <SweetAlert
      show={show}
      confirmBtnStyle={{ display: "none" }}
      customClass="sign-in"
      title={""}
      onConfirm={() => console.log("")}
    >
      <form
        className="login"
        action="https://echo.htmlacademy.ru"
        method="post"
      >
        <button
          className="login__exit-btn"
          type="button"
          onClick={() => setShowLoginModal(false)}
        >
        </button>

        <div className="login__container">
          <h2 className="login__headline">Добро пожаловать</h2>
          <p className="login__txt">
            Войдите с вашим номером телефона или паролем
          </p>

          <input
            className="login__input"
            // ref={telRef}
            type="tel"
            defaultValue={"+998 "}
            name="phone_number"
            maxLength={14}
          />
          <button className="login__submit-btn" type="submit">
            Получить код
          </button>

          <p className="login__txt">Или войти с</p>

          <div className="login__links-wrapper">
            <a className="login__login-link google-link" href="#"></a>
            <a className="login__login-link facebook-link" href="#"></a>
          </div>

          <p className="login__txt login__subtext">
            У вас нет аккаунта?
            <button
              className="login__link"
              type="button"
              onClick={() => {
                setIsRegistered(false);
              }}
            >
              Зарегистрируйтесь
            </button>
          </p>
        </div>
      </form>
    </SweetAlert>
  );
};

export default SignIn;
